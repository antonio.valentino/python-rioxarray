Source: python-rioxarray
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Magnus Hagdorn <magnus.hagdorn@ed.ac.uk>,
           Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Priority: optional
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Build-Depends: debhelper-compat (= 12),
               dh-sequence-python3,
               dh-python,
               pandoc,
               python3-all,
               python3-rasterio,
               python3-scipy,
               python3-xarray (>= 0.17),
               python3-pyproj (>= 2.2),
               python3-pytest,
               python3-pytest-cov,
               python3-pytest-timeout,
               python3-netcdf4,
               python3-mock,
               python3-dask
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/python-rioxarray
Vcs-Git: https://salsa.debian.org/science-team/python-rioxarray.git
Homepage: https://github.com/corteva/rioxarray

Package: python3-rioxarray
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-scipy,
         python3-rasterio,
         python3-xarray (>= 0.17),
         python3-pyproj (>= 2.2)
Description: rasterio xarray extension
 rioxarray is an extension to the xarray package that
  - supports multidimensional datasets such as netCDF
  - stores the CRS as a WKT, which is the recommended format
  - loads in the CRS, transform, and nodata metadata in standard CF & GDAL
    locations
  - supports masking and scaling data with the masked and mask_and_scale kwargs
  - adds the coordinate axis CF metadata
  - loads in raster metadata into the attributes
 .
 This package provides the Python 3 library
